﻿using Avigma.Repository.Lib;
using Avigma.Repository.Security;
using API.Models.Project;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace API.Repository.Project
{
    public class User_Post_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        SecurityHelper securityHelper = new SecurityHelper();

        private List<dynamic> CreateUpdate_User_Post(User_Post_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_User_Post]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@UP_PKeyID", 1 + "#bigint#" + model.UP_PKeyID);
                input_parameters.Add("@UP_ImageName", 1 + "#nvarchar#" + model.UP_ImageName);
                input_parameters.Add("@UP_Size", 1 + "#int#" + model.UP_Size);
                input_parameters.Add("@UP_ImagePath", 1 + "#nvarchar#" + model.UP_ImagePath);
                input_parameters.Add("@UP_IsFirst", 1 + "#bit#" + model.UP_IsFirst);
                input_parameters.Add("@UP_Number", 1 + "#int#" + model.UP_Number);
                input_parameters.Add("@UP_UserID", 1 + "#bigint#" + model.UP_UserID);
                input_parameters.Add("@UP_UC_PKeyID", 1 + "#bigint#" + model.UP_UC_PKeyID);
                input_parameters.Add("@UP_COLL_PKeyID", 1 + "#bigint#" + model.UP_COLL_PKeyID);
                input_parameters.Add("@UP_Show", 1 + "#int#" + model.UP_Show);
                input_parameters.Add("@UP_AddSpotlight", 1 + "#bit#" + model.UP_AddSpotlight);
                input_parameters.Add("@UP_Closet", 1 + "#bit#" + model.UP_Closet);
                input_parameters.Add("@UP_Product_URL", 1 + "#nvarchar#" + model.UP_Product_URL);
                input_parameters.Add("@UP_Product_URL_1", 1 + "#nvarchar#" + model.UP_Product_URL_1);
                input_parameters.Add("@UP_Product_URL_2", 1 + "#nvarchar#" + model.UP_Product_URL_2);
                input_parameters.Add("@UP_Product_URL_3", 1 + "#nvarchar#" + model.UP_Product_URL_3);
                input_parameters.Add("@UP_Product_URL_4", 1 + "#nvarchar#" + model.UP_Product_URL_4);
                input_parameters.Add("@UP_Coll_Desc", 1 + "#nvarchar#" + model.UP_Coll_Desc);
                input_parameters.Add("@UP_IsAdmin", 1 + "#bit#" + model.UP_IsAdmin);
                input_parameters.Add("@UP_IsActive", 1 + "#bit#" + model.UP_IsActive);
                input_parameters.Add("@UP_IsDelete", 1 + "#bit#" + model.UP_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@UP_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        private DataSet Get_User_Post(User_Post_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_User_Post]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@UP_PKeyID", 1 + "#bigint#" + model.UP_PKeyID);
                input_parameters.Add("@UP_UserID", 1 + "#bigint#" + model.UP_UserID);
                input_parameters.Add("@UP_UC_PKeyID", 1 + "#bigint#" + model.UP_UC_PKeyID);
                input_parameters.Add("@UP_COLL_PKeyID", 1 + "#bigint#" + model.UP_COLL_PKeyID);
                input_parameters.Add("@UP_AddSpotlight", 1 + "#bit#" + model.UP_AddSpotlight);
                input_parameters.Add("@UP_Closet", 1 + "#bit#" + model.UP_Closet);
                input_parameters.Add("@COLL_PKeyID", 1 + "#bigint#" + model.COLL_PKeyID);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@NoofRows", 1 + "#int#" + model.NoofRows);
                input_parameters.Add("@Orderby", 1 + "#varchar#" + model.Orderby);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }

        public List<dynamic> CreateUpdate_User_Post_DataDetails(User_Post_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdate_User_Post(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }

        public List<dynamic> Get_User_PostDetails(User_Post_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = Get_User_Post(model);

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }
                    
                }
                //objDynamic.Add(Get_details);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }

        private DataSet Get_User_Post(CollectionDropdownDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_DropDown_Collection]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@COLL_PKeyID", 1 + "#bigint#" + model.COLL_PKeyID);
              
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }

        public List<dynamic> Get_DropDown_CollectionDetails(CollectionDropdownDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_User_Post(model);
                if (model.Type == 1)
                {


                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<CollectionDropdownDTO> PCRdrdDamge =
                       (from item in myEnumerableFeaprd
                        select new CollectionDropdownDTO
                        {
                            COLL_PKeyID = item.Field<Int64>("COLL_PKeyID"),
                            COLL_Name = item.Field<String>("COLL_Name"),



                        }).ToList();

                    objDynamic.Add(PCRdrdDamge);

                }
                
                

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}