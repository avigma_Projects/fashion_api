﻿using Avigma.Repository.Lib;
using Avigma.Repository.Security;
using API.Models.Project;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace API.Repository.Project
{
    public class User_Favorite_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        SecurityHelper securityHelper = new SecurityHelper();

        private List<dynamic> CreateUpdate_User_Favorite(User_Favorite_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_User_Favorite]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@UF_Pkey", 1 + "#bigint#" + model.UF_Pkey);
                input_parameters.Add("@UF_User_PkeyID", 1 + "#bigint#" + model.UF_User_PkeyID);
                input_parameters.Add("@UF_UP_PKeyID", 1 + "#bigint#" + model.UF_UP_PKeyID);
                input_parameters.Add("@UF_UC_PKeyID", 1 + "#bigint#" + model.UF_UC_PKeyID);
                input_parameters.Add("@UC_Name", 1 + "#nvarchar#" + model.UC_Name);
                input_parameters.Add("@UF_Closet_Spotlight", 1 + "#int#" + model.UF_Closet_Spotlight);
                input_parameters.Add("@UF_IsActive", 1 + "#bit#" + model.UF_IsActive);
                input_parameters.Add("@UF_IsDelete", 1 + "#bit#" + model.UF_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@UF_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        private DataSet Get_User_Favorite(User_Favorite_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_User_Favorite]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@UF_Pkey", 1 + "#bigint#" + model.UF_Pkey);
                input_parameters.Add("@UF_User_PkeyID", 1 + "#bigint#" + model.UF_User_PkeyID);
                input_parameters.Add("@UF_UP_PKeyID", 1 + "#bigint#" + model.UF_UP_PKeyID);
                input_parameters.Add("@UF_UC_PKeyID", 1 + "#bigint#" + model.UF_UC_PKeyID);
                
                input_parameters.Add("@UF_Closet_Spotlight", 1 + "#int#" + model.UF_Closet_Spotlight);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }

        public List<dynamic> CreateUpdate_User_FavoriteDetails(User_Favorite_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdate_User_Favorite(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }

        public List<dynamic> Get_User_FavoriteDetails(User_Favorite_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = Get_User_Favorite(model);

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }
                   
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }
    }
}