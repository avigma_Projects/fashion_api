﻿using Avigma.Repository.Lib;
using Avigma.Repository.Security;
using Avigma.Repository.Lib.FireBase;
using API.Models.Project;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace API.Repository.Project
{
    public class User_Followers_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        SecurityHelper securityHelper = new SecurityHelper();

        private List<dynamic> CreateUpdate_User_Followers(User_Followers_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_User_Followers]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@FLL_PKeyID", 1 + "#bigint#" + model.FLL_PKeyID);
                input_parameters.Add("@FLL_My_UserID", 1 + "#bigint#" + model.FLL_My_UserID);
                input_parameters.Add("@FLL_UserID", 1 + "#bigint#" + model.FLL_UserID);
                input_parameters.Add("@FLL_IsAccepted", 1 + "#int#" + model.FLL_IsAccepted);
                input_parameters.Add("@FLL_IsActive", 1 + "#bit#" + model.FLL_IsActive);
                input_parameters.Add("@FLL_IsDelete", 1 + "#bit#" + model.FLL_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@FLL_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        private DataSet Get_User_Followers(User_Followers_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_User_Followers]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@FLL_PKeyID", 1 + "#bigint#" + model.FLL_PKeyID);
                input_parameters.Add("@FLL_My_UserID", 1 + "#bigint#" + model.FLL_My_UserID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }


        public List<dynamic> CreateUpdate_User_Followers_DataDetails(User_Followers_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdate_User_Followers(model);
                if (model.Type ==1)
                {
                    NotificationGetData notificationGet = new NotificationGetData();
                    string UserToken = string.Empty, message = string.Empty, msgtitle = string.Empty;
                    User_Followers_DTO user_Followers_DTO = new User_Followers_DTO();
                    user_Followers_DTO.FLL_My_UserID = model.UserID;
                    user_Followers_DTO.UserID = model.FLL_UserID;
                    user_Followers_DTO.Type = 6;
                    DataSet ds = Get_User_Followers(user_Followers_DTO);
                    for (int j = 0; j < ds.Tables[1].Rows.Count; j++)
                    {
                        message = "Notification Received Follower Request  from " + ds.Tables[1].Rows[j]["User_Name"].ToString();
                        msgtitle = "Notification Received Follower Request from" + ds.Tables[1].Rows[j]["User_Name"].ToString();
                    }
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        UserToken = ds.Tables[0].Rows[i]["User_Token_val"].ToString();
                       
                        if (!string.IsNullOrWhiteSpace(UserToken))
                        {
                            notificationGet.SendNotification(UserToken, message, msgtitle, "1");
                        }
                        else
                        {
                            log.logInfoMessage("Token is Null for User---->" + ds.Tables[0].Rows[i]["User_PkeyID"].ToString() + "----->" +ds.Tables[0].Rows[i]["User_Name"].ToString());
                        }
                       
                    }
                   
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }

        public List<dynamic> Get_User_FollowersDetails(User_Followers_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = Get_User_Followers(model);

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }
    }
}