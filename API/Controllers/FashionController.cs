﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Avigma.Repository.Lib;
using API.Models.Project;
using API.Repository.Project;
using Avigma.Models;
using RestSharp;
using System.Configuration;
using System.Web;
using System.IO;

namespace API.Controllers
{
    [RoutePrefix("api/Fashion")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FashionController : BaseController
    {
        Log log = new Log();
        User_Admin_Master_Data User_Admin_Master_Data = new User_Admin_Master_Data();
        Get_User_Admin_Role_Data Get_User_Admin_Role_Data = new Get_User_Admin_Role_Data();
        UserMaster_Data userMaster_Data = new UserMaster_Data();
        Collection_Master_Data collection_Master_Data = new Collection_Master_Data();
        User_Collection_Data user_Collection_Data = new User_Collection_Data();
        User_Post_Data user_Post_Data = new User_Post_Data();
        User_Followers_Data user_Followers_Data = new User_Followers_Data();
        Menu_Master_Data Menu_Master_Data = new Menu_Master_Data();
        Menu_Role_Relation_Data Menu_Role_Relation_Data = new Menu_Role_Relation_Data();
        User_Favorite_Data User_Favorite_Data = new User_Favorite_Data();
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("AddUserMasterData")]
        public async Task<List<dynamic>> AddUserMasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {



                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<UserMaster_DTO>(s);
                Data.UserID = LoggedInUserId;
                if (Data.Type != 1)
                {
                    if (Data.Type == 8)
                    {
                        Data.Type = 2;
                    }
                    else if (Data.Type != 9 && Data.Type != 4)
                    {
                        Data.User_PkeyID = LoggedInUserId;
                    }
                    else
                    {
                        //Data.User_PkeyID = LoggedInUserId;
                    }

                }
                var userMasterDetails = await Task.Run(() => userMaster_Data.AddUserMaster_Data(Data));

                return userMasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetUserMasterData")]
        public async Task<List<dynamic>> GetUserMasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {

                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------GetUserMasterData Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------GetUserMasterData End--------------");
                var Data = JsonConvert.DeserializeObject<UserMaster_DTO>(s);
                Data.UserID = LoggedInUserId;
                if (Data.Type == 2)
                {
                    Data.User_PkeyID = LoggedInUserId;
                }
                else
                {
                    if (Data.Type != 1)
                    {
                        if (Data.Type == 4)
                        {
                            Data.Type = 2;
                        }
                        else
                        {
                            // Data.User_PkeyID = LoggedInUserId;
                        }

                    }
                }
              
                
               
                var userMasterDetails = await Task.Run(() => userMaster_Data.Get_UserMasterDetails(Data));

                return userMasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("UploadImages")]
        public async Task<List<dynamic>> UploadImages()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {

                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<ImageData>(s);
                ImageGenerator imageGenerator = new ImageGenerator();
                var ImageData = await Task.Run(() => imageGenerator.GetImagePath(Data));

                return ImageData;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("UploadDocuments")]
        public IHttpActionResult UploadDocuments()
        {
            Response res = new Response();
            res.Code = 401;
            res.Data = "";
            res.Message = "Unable to upload file";
            string BasePath = ConfigurationManager.AppSettings["DocumentPath"];
            string LocalBasePath = ConfigurationManager.AppSettings["DocumentDBPath"];

            try
            {

                var files = HttpContext.Current.Request.Files;

                if (HttpContext.Current.Request.Files.Count > 0)
                {
                    List<object> filePathList = new List<object>();
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFile postedFile = files[i];
                        string filePath = string.Empty;
                        if (postedFile != null)
                        {
                            string path = System.Web.HttpContext.Current.Server.MapPath("~/UploadDocuments/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            var extension = Path.GetExtension(postedFile.FileName);
                            var FileName2 = "";
                            if (extension == null || extension == "")
                            {
                                FileName2 = DateTime.Now.Ticks + "_" + i + ".pdf";
                            }
                            else
                            {
                                FileName2 = DateTime.Now.Ticks + "_" + i + extension;
                            }
                            //var FileName2 = DateTime.Now.Ticks + extension;
                            filePath = path + FileName2;
                            postedFile.SaveAs(filePath);
                            var FileName1 = Path.GetFileName(postedFile.FileName);



                            //string FileUrl = BasePath + FileName2;
                            filePathList.Add(new { url = LocalBasePath + FileName2, name = FileName1, ext = extension });
                        }
                    }


                    res.Code = 200;
                    res.Error = "";
                    res.Data = filePathList;
                    res.Message = "Success...!";
                    return Json(res);
                }
            }
            catch (Exception ex)
            {
                res.Code = 400;
                res.Error = "";
                res.Data = "";
                res.Message = ex.Message;
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return Json(res);
            }
            return Json("");
        }

        [HttpPost]
        [Route("ForGotPassword")]
        public async Task<List<dynamic>> ForGotPassword()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                GetSetUser getSetUser = new GetSetUser();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------ForGotPassword Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------ForGotPassword End--------------");
                var user_Child_DTO = JsonConvert.DeserializeObject<UserLogin>(s);


                var MasterDetails = await Task.Run(() => getSetUser.GetForGetPassword(user_Child_DTO));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetVerificationLink")]
        public async Task<List<dynamic>> GetVerificationLink()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                GetSetUser getSetUser = new GetSetUser();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------GetVerificationLink Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------GetVerificationLink End--------------");
                var user_Child_DTO = JsonConvert.DeserializeObject<UserLogin>(s);
                user_Child_DTO.User_PkeyID = LoggedInUserId;
                user_Child_DTO.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => getSetUser.GetVerificationLink(user_Child_DTO));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [HttpPost]
        [Route("GetUserViryficationDetails")]
        public async Task<List<dynamic>> GetUserViryficationDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                UserVerificationMaster_Data userVerificationMaster_Data = new UserVerificationMaster_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------GetUserViryficationDetails Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------GetUserViryficationDetails End--------------");
                //log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<UserVerificationMaster_DTO>(s);
                //Data.Type = 1;

                var Getuser = await Task.Run(() => userVerificationMaster_Data.Check_User(Data));
                return Getuser;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [HttpPost]
        [Route("VerifyUserByEmail")]
        public async Task<List<dynamic>> VerifyUserByEmail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                UserVerificationMaster_Data userVerificationMaster_Data = new UserVerificationMaster_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------VerifyUserByEmail Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------VerifyUserByEmail End--------------");

                var Data = JsonConvert.DeserializeObject<UserMaster_DTO>(s);


                var Getuser = await Task.Run(() => userMaster_Data.VerifyUserByEmail(Data));
                return Getuser;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("ChangePassword")]
        public async Task<List<dynamic>> ChangePassword()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                UserMaster_Data userMaster_Data = new UserMaster_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------ChangePassword Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------ChangePassword End--------------");
                var userMaster_DTO = JsonConvert.DeserializeObject<UserMaster_ChangePassword>(s);

                var userDetails = await Task.Run(() => userMaster_Data.ChangePassword(userMaster_DTO));
                userMaster_DTO.Type = 5;
                return userDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("ChangePasswordByEmail")]
        public async Task<List<dynamic>> ChangePasswordByEmail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                UserMaster_Data userMaster_Data = new UserMaster_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------ChangePassword Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------ChangePassword End--------------");
                var userMaster_DTO = JsonConvert.DeserializeObject<UserMaster_ChangePassword>(s);

                var userDetails = await Task.Run(() => userMaster_Data.ChangePasswordByEmail(userMaster_DTO));
                userMaster_DTO.Type = 5;
                return userDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("DeleteUserMaster")]
        public async Task<List<dynamic>> DeleteUserMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                UserMaster_Data userMaster_Data = new UserMaster_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<UserMaster_DTO>(s);
                Data.UserID = LoggedInUserId;
                Data.User_PkeyID = LoggedInUserId;
                var userMasterDetails = await Task.Run(() => userMaster_Data.DeleteUserMasterDetails(Data));

                return userMasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("CreateUpdateUserAdminMaster")]
        public async Task<List<dynamic>> CreateUpdate_User_Admin_Master()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------CreateUpdateUserAdminMaster Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------CreateUpdateUserAdminMaster End--------------");
                var Data = JsonConvert.DeserializeObject<User_Admin_Master_DTO>(s);
                Data.UserID = LoggedInUserId;

                var CreateUpdate_User_Admin_Master_DataDetails = await Task.Run(() => User_Admin_Master_Data.CreateUpdate_User_Admin_Master_DataDetails(Data));

                return CreateUpdate_User_Admin_Master_DataDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("CreateUpdateUserAdminMaster");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetUserAdminMaster")]
        public async Task<List<dynamic>> Get_User_Admin_Master()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------GetUserAdminMaster Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------GetUserAdminMaster End--------------");
                var Data = JsonConvert.DeserializeObject<User_Admin_Master_DTO>(s);
                Data.UserID = LoggedInUserId;

                var Get_User_Admin_MasterDetails = await Task.Run(() => User_Admin_Master_Data.Get_User_Admin_MasterDetails(Data));

                return Get_User_Admin_MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("GetUserAdminMaster");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("CreateUpdateCollectionMaster")]
        public async Task<List<dynamic>> CreateUpdate_Collection_Master()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------CreateUpdateCollectionMaster Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------CreateUpdateCollectionMaster End--------------");
                var Data = JsonConvert.DeserializeObject<Collection_Master_DTO>(s);
                Data.UserID = LoggedInUserId;

                var CreateUpdate_Collection_Master_DataDetails = await Task.Run(() => collection_Master_Data.CreateUpdate_Collection_Master_DataDetails(Data));

                return CreateUpdate_Collection_Master_DataDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("CreateUpdateCollectionMaster");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetCollectionMaster")]
        public async Task<List<dynamic>> Get_Collection_Master()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------GetCollectionMaster Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------GetCollectionMaster End--------------");
                var Data = JsonConvert.DeserializeObject<Collection_Master_DTO>(s);
                Data.UserID = LoggedInUserId;

                var Get_Collection_MasterDetails = await Task.Run(() => collection_Master_Data.Get_Collection_MasterDetails(Data));

                return Get_Collection_MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("GetCollectionMaster");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("CreateUpdateUserCollection")]
        public async Task<List<dynamic>> CreateUpdate_User_Collection()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------CreateUpdateUserCollection Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------CreateUpdateUserCollection End--------------");
                var Data = JsonConvert.DeserializeObject<User_Collection_DTO>(s);
                Data.UserID = LoggedInUserId;
                Data.UC_UserID = LoggedInUserId;
                var CreateUpdate_User_Collection_DataDetails = await Task.Run(() => user_Collection_Data.CreateUpdate_User_Collection_DataDetails(Data));

                return CreateUpdate_User_Collection_DataDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("CreateUpdateUserCollection");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetUserCollection")]
        public async Task<List<dynamic>> Get_User_Collection()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------GetUserCollection Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------GetUserCollection End--------------");
                var Data = JsonConvert.DeserializeObject<User_Collection_DTO>(s);
                Data.UserID = LoggedInUserId;
                Data.UC_UserID = Data.UserID;
                var Get_User_CollectionDetails = await Task.Run(() => user_Collection_Data.Get_User_CollectionDetails(Data));

                return Get_User_CollectionDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("GetUserCollection");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("CreateUpdateUserPost")]
        public async Task<List<dynamic>> CreateUpdate_User_Post()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------CreateUpdateUserPost Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------CreateUpdateUserPost End--------------");
                var Data = JsonConvert.DeserializeObject<User_Post_DTO>(s);
                Data.UserID = LoggedInUserId;
                Data.UP_UserID = Data.UserID;
                var CreateUpdate_User_Post_DataDetails = await Task.Run(() => user_Post_Data.CreateUpdate_User_Post_DataDetails(Data));

                return CreateUpdate_User_Post_DataDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("CreateUpdateUserPost");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetUserPost")]
        public async Task<List<dynamic>> Get_User_Post()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            string s = string.Empty;
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                 s = reader.ReadToEnd();
                log.logDebugMessage("----------GetUserPost Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------GetUserPost End--------------");
                var Data = JsonConvert.DeserializeObject<User_Post_DTO>(s);
                Data.UserID = LoggedInUserId;
                Data.UP_UserID = Data.UserID; 
                var Get_User_PostDetails = await Task.Run(() => user_Post_Data.Get_User_PostDetails(Data));

                return Get_User_PostDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("GetUserPost");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(s);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("CreateUpdateUserFollowers")]
        public async Task<List<dynamic>> CreateUpdate_User_Followers()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------CreateUpdateUserFollowers Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------CreateUpdateUserFollowers End--------------");
                var Data = JsonConvert.DeserializeObject<User_Followers_DTO>(s);
                Data.UserID = LoggedInUserId;

                var CreateUpdate_User_Followers_DataDetails = await Task.Run(() => user_Followers_Data.CreateUpdate_User_Followers_DataDetails(Data));

                return CreateUpdate_User_Followers_DataDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("CreateUpdateUserFollowers");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetUserFollowers")]
        public async Task<List<dynamic>> Get_User_Followers()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------GetUserFollowers Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------GetUserFollowers End--------------");
                var Data = JsonConvert.DeserializeObject<User_Followers_DTO>(s);
                Data.UserID = LoggedInUserId;

                var Get_User_FollowersDetails = await Task.Run(() => user_Followers_Data.Get_User_FollowersDetails(Data));

                return Get_User_FollowersDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("GetUserFollowers");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetUserAdminRole")]
        public async Task<List<dynamic>> Get_User_Admin_Role()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------GetUserAdminRole Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------GetUserAdminRole End--------------");
                var Data = JsonConvert.DeserializeObject<User_Admin_Master_DTO>(s);
                Data.UserID = LoggedInUserId;

                var Get_User_Admin_RoleDetails = await Task.Run(() => Get_User_Admin_Role_Data.Get_User_Admin_RoleDetails(Data));

                return Get_User_Admin_RoleDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("GetUserAdminRole");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("CreateUpdateMenuMaster")]
        public async Task<List<dynamic>> CreateUpdate_Menu_Master()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------CreateUpdateMenuMaster Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------CreateUpdateMenuMaster End--------------");
                var Data = JsonConvert.DeserializeObject<Menu_Master_DTO>(s);
                Data.UserID = LoggedInUserId;

                var CreateUpdate_Menu_Master_DataDetails = await Task.Run(() => Menu_Master_Data.CreateUpdate_Menu_Master_DataDetails(Data));

                return CreateUpdate_Menu_Master_DataDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("CreateUpdateMenuMaster");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetMenuMaster")]
        public async Task<List<dynamic>> Get_Menu_Master()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------GetMenuMaster Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------GetMenuMaster End--------------");
                var Data = JsonConvert.DeserializeObject<Menu_Master_DTO>(s);
                Data.UserID = LoggedInUserId;

                var Get_Menu_MasterDetails = await Task.Run(() => Menu_Master_Data.Get_Menu_MasterDetails(Data));

                return Get_Menu_MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("GetMenuMaster");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("CreateUpdateMenuRoleRelation")]
        public async Task<List<dynamic>> CreateUpdate_Menu_Role_Relation()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------CreateUpdateMenuRoleRelation Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------CreateUpdateMenuRoleRelation End--------------");
                var Data = JsonConvert.DeserializeObject<Menu_Role_Relation_DTO>(s);
                Data.UserID = LoggedInUserId;

                var CreateUpdate_Menu_Role_Relation_DataDetails = await Task.Run(() => Menu_Role_Relation_Data.CreateUpdate_Menu_Role_Relation_DataDetails(Data));

                return CreateUpdate_Menu_Role_Relation_DataDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("CreateUpdateMenuRoleRelation");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetMenuRoleRelation")]
        public async Task<List<dynamic>> Get_Menu_Role_Relation()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------GetMenuRoleRelation Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------GetMenuRoleRelation End--------------");
                var Data = JsonConvert.DeserializeObject<Menu_Role_Relation_DTO>(s);
                Data.UserID = LoggedInUserId;

                var Get_Menu_Role_RelationDetails = await Task.Run(() => Menu_Role_Relation_Data.Get_Menu_Role_RelationDetails(Data));

                return Get_Menu_Role_RelationDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(" GetMenuRoleRelation");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }



        }


        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("CreateUpdateUserFavorite")]
        public async Task<List<dynamic>> CreateUpdateUserFavorite()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------CreateUpdateUserFavorite Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------CreateUpdateUserFavorite End--------------");
                var Data = JsonConvert.DeserializeObject<User_Favorite_DTO>(s);
                Data.UserID = LoggedInUserId;
                Data.UF_User_PkeyID = LoggedInUserId;
                var CreateUpdate_User_Followers_DataDetails = await Task.Run(() => User_Favorite_Data.CreateUpdate_User_FavoriteDetails(Data));

                return CreateUpdate_User_Followers_DataDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("CreateUpdateUserFollowers");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetUserFavorite")]
        public async Task<List<dynamic>> GetUserFavorite()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------GetUserFavorite Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------GetUserFavorite End--------------");
                var Data = JsonConvert.DeserializeObject<User_Favorite_DTO>(s);
                Data.UserID = LoggedInUserId;
                Data.UF_User_PkeyID = LoggedInUserId;
                var Get_User_FollowersDetails = await Task.Run(() => User_Favorite_Data.Get_User_FavoriteDetails(Data));

                return Get_User_FollowersDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("GetUserFavorite");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetDropDownCollection")]
        public async Task<List<dynamic>> Get_DropDown_Collection()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------GetDropDownCollection Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------GetDropDownCollection End--------------");
                var Data = JsonConvert.DeserializeObject<CollectionDropdownDTO>(s);
                Data.UserID = LoggedInUserId;
               
                var Get_User_PostDetails = await Task.Run(() => user_Post_Data.Get_DropDown_CollectionDetails(Data));

                return Get_User_PostDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("GetDropDownCollection");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

    }
}