﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Project
{
    public class User_Collection_DTO
    {
        public Int64 UC_PKeyID { get; set; }
        public String UC_Name { get; set; }
        public String UC_Description { get; set; }
        public Int64? UC_UserID { get; set; }
        public Int64? UC_COLL_PKeyID { get; set; }
        public Boolean? UC_Show { get; set; }
        public Boolean? UC_IsActive { get; set; }
        public Boolean? UC_IsDelete { get; set; }
        public int? Type { get; set; }
        public int? UC_Closet_Spotlight { get; set; }
        public Int64? UserID { get; set; }
        public String WhereClause { get; set; }

    }
}