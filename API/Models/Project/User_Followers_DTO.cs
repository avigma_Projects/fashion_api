﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Project
{
    public class User_Followers_DTO
    {
        public Int64 FLL_PKeyID { get; set; }
        public Int64? FLL_My_UserID { get; set; }
        public Int64? FLL_UserID { get; set; }
        public int? FLL_IsAccepted { get; set; }
        public Boolean? FLL_IsActive { get; set; }
        public Boolean? FLL_IsDelete { get; set; }
        public int? Type { get; set; }
        public Int64? UserID { get; set; }

 
    }
}