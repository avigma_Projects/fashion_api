﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Project
{
    public class User_Post_DTO
    {

        public Int64 UP_PKeyID { get; set; }
        public String UP_ImageName { get; set; }
        public int? UP_Size { get; set; }
        public String UP_ImagePath { get; set; }
        public Boolean? UP_IsFirst { get; set; }
        public int? UP_Number { get; set; }
        public Int64? UP_UserID { get; set; }
        public Int64? UP_UC_PKeyID { get; set; }
        public Int64? UP_COLL_PKeyID { get; set; }
        public int? UP_Show { get; set; }
        public Boolean? UP_AddSpotlight { get; set; }
        public Boolean? UP_Closet { get; set; }
        public String UP_Product_URL { get; set; }
        public String UP_Product_URL_1 { get; set; }
        public String UP_Product_URL_2 { get; set; }
        public String UP_Product_URL_3 { get; set; }
        public String UP_Product_URL_4 { get; set; }
        public String UP_Coll_Desc { get; set; }
        public String WhereClause { get; set; }
        public String Orderby { get; set; }
        public Int64 COLL_PKeyID { get; set; }
        public Boolean? UP_IsActive { get; set; }
        public Boolean? UP_IsAdmin { get; set; }
        public Boolean? UP_IsDelete { get; set; }
        public String COLL_Name { get; set; }
        public int? PageNumber { get; set; }
        public int? NoofRows { get; set; }
        public int? Type { get; set; }
        public Int64? UserID { get; set; }
    }


    public class CollectionDropdownDTO
    {
        public Int64 COLL_PKeyID { get; set; }
        public String COLL_Name { get; set; }
        public Boolean? COLL_IsActive { get; set; }
        public Boolean? COLL_IsDelete { get; set; }
        public int? Type { get; set; }
        public Int64? UserID { get; set; }

    }
}