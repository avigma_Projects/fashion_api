﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Project
{
    public class Collection_Master_DTO
    {
        public Int64 COLL_PKeyID { get; set; }
        public String COLL_Name { get; set; }
        public String COLL_Description { get; set; }
        public Boolean? COLL_IsActive { get; set; }
        public Boolean? COLL_IsDelete { get; set; }
        public int? Type { get; set; }
        public Int64? UserID { get; set; }

    }
}