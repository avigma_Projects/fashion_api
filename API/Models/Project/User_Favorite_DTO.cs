﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Project
{
    public class User_Favorite_DTO
    {
        public Int64 UF_Pkey { get; set; }
        public Int64 UF_User_PkeyID { get; set; }
        public Int64 UF_UP_PKeyID { get; set; }
        public Int64 UF_UC_PKeyID { get; set; }
        public String UC_Name { get; set; }
        public int? UF_Closet_Spotlight { get; set; }
        public Boolean? UF_IsActive { get; set; }
        public Boolean? UF_IsDelete { get; set; }
        public int? Type { get; set; }
        public Int64? UserID { get; set; }
        public String WhereClause { get; set; }
    }
}